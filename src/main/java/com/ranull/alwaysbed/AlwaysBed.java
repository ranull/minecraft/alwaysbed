package com.ranull.alwaysbed;

import com.ranull.alwaysbed.events.Events;
import org.bukkit.plugin.java.JavaPlugin;

public final class AlwaysBed extends JavaPlugin {
    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(new Events(), this);
    }
}
